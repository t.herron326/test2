const express = require('express')
const app = express()
const port = 8080
require('dotenv').config();
//
let name = process.env.USER;
let testrun = null;
//
app.get('/', function (req, res) {
    if (testrun != true) {
        res.sendFile(__dirname + "/" + "test.html");
        testrun = true;
    } else {
        res.sendFile(__dirname + "/" + "layout.html");
        testrun = false;
    }
})
app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})
